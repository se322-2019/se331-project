
import {RouterModule, Routes} from '@angular/router';
import { NgModule } from '@angular/core';
import { StudentsViewComponent } from './students/view/students.view.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsComponent } from './students/list/students.component';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';


const appRoutes : Routes = [
    
    {path : 'view',component : StudentsViewComponent},
    {path : 'add' , component : StudentsAddComponent},
    {path : 'list', component : StudentsComponent},
    {path : '**', component : FileNotFoundComponent}

];

@NgModule({
imports: [
    RouterModule.forRoot(appRoutes)
],
exports:[
    RouterModule
]
})
export class AppRoutingModule { }


